<?php
/**
 * Template Name: Learn
 * @package Spike
 */

get_header();

$fields = get_fields();

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main home" role="main">

			<section class="quote">
				<div class="contain">
					<img src="<?= get_stylesheet_directory_uri() . '/img/quotes/hamilton.png';?>"/>
          <?php include( get_stylesheet_directory() . '/svg/quote-ham.svg') ?>
				</div>
			</section>

      <section class="infographic">
        <div class="contain">
					<?= $fields['white_body'] ?>
        </div>
      </section>

			<section class="impact">
				<div class="contain">
					<?= $fields['blue_body'] ?>
				</div>
			</section>
			
			<section class="content">
				<div class="contain">
					<?= $fields['white_body_two'] ?>
				</div>
			</section>
			
			<section class="impact">
				<div class="contain">
					<?= $fields['blue_body_two'] ?>
				</div>
			</section>

			<?php if ($fields['show_videos'][0] == "show videos"): ?>
			<section class="video-gallary">
				<div class="contain">
					<?= do_shortcode('[videos amount="all"]'); ?>
				</div>
			</section>
			<?php endif; ?>

			
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
