<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Spike
 */
?>

	</div><!-- #content -->

	<footer class="site-footer">
		<div class="contain">
			<div class="spread">
				<?php
					$url = get_home_url();
					$title = urlencode(get_bloginfo('description'));
				?>
				<p>
					<span>Help spread the word</span>
					<?php printf('<a class="icon fb" href="https://www.facebook.com/sharer/sharer.php?u=%s&t=%s" target="_blank" title="Share on Facebook"></a>', $url, $title); ?>
					<?php printf('<a class="icon tw" href="https://twitter.com/share?url=%s&text=%s" target="_blank" title="Share on Twitter"></a>', $url, $title); ?>
					<?php printf('<a class="icon insta" href="https://www.instagram.com/protectfldemocracy/?hl=en" title="Check out our Instagram"></a>'); ?>
				</p>

			</div>
			<?php include( get_stylesheet_directory() . '/svg/logo.svg') ?>
		</div>
	</footer>
	<footer class="copyright">
		<div class="contain"> 
			<span class="default-font">&copy; </span><?php dynamic_sidebar("footer"); ?>
		</div>
	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

<div class="video-modal-background hidden" id="js-video-modal-bg"></div>
<div class="video-modal hidden" id="js-video-modal">
	<div class="contain">
		<div class="video-wrapper square">
			<button class="close simple">
				<?php include( get_stylesheet_directory() . '/svg/close.svg') ?>
			</button>
		</div>
	</div>
</div>

<script src="<?= get_stylesheet_directory_uri(); ?>/js/site.min.js" async defer ></script>

</body>
</html>
