<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Spike
 */

$fields = get_fields();

get_header();?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<section class="quote top-section">
					<div class="contain">
						<blockquote>
							<p><?= $fields['quote'] ?></p>
							<footer>– <cite><?= $fields['credit'] ?></cite></footer>
						</blockquote>
					</div>
				</section>
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header><!-- .entry-header -->
			
				<div class="entry-content contain">
					<?php the_content(); ?>
				</div><!-- .entry-content -->
			</article><!-- #post-## -->
	
	<?php endwhile; // end of the loop. ?>
	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
