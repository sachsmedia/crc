var videos = document.querySelectorAll('.video-wrapper.inline');
// var fullscreens = document.querySelectorAll('.fullscreen');

for (var i = 0; i < videos.length; i++) {
  // videos[i].addEventListener('click', togglePlay, false);
  videos[i].firstElementChild.addEventListener('playing', toggleButton, false);
  videos[i].firstElementChild.addEventListener('pause', toggleButton, false);
  
  videos[i].addEventListener('webkitfullscreenchange', upgradeQuality, false);
  videos[i].addEventListener('webkitbeginfullscreen', upgradeQuality, false);
  videos[i].addEventListener('mozfullscreenchange', upgradeQuality, false);
  videos[i].addEventListener('fullscreenchange', upgradeQuality, false);
}
document.addEventListener('resize', upgradeQuality, false);
// for (var i = 0; i < fullscreens.length; i++) {
//   fullscreens[i].addEventListener("click", goFullScreen, false);
// }

function toggleButton() {
  this.parentElement.classList.toggle("playing");
}

function togglePlay() {  
  var vid = this.firstElementChild;
  if (vid.paused) {
    vid.play();
    vid.addEventListener('ended', toggleButton, false);
  }else{
    vid.pause();
    vid.removeEventListener('ended', toggleButton, false);
  }
}

// function goFullScreen(event) {
//   this.parentElement.firstElementChild.webkitRequestFullScreen();
//   event.stopPropagation();
// }

function upgradeQuality() {
  // if ( !(document.fullscreen || document.mozFullScreen ||  document.webkitIsFullScreen)) return;

  console.log("upgrading");
  this.removeEventListener('webkitfullscreenchange', upgradeQuality, false);
  this.removeEventListener('webkitbeginfullscreen', upgradeQuality, false);
  this.removeEventListener('mozfullscreenchange', upgradeQuality, false);
  this.removeEventListener('fullscreenchange', upgradeQuality, false);

  
  var vid = this.firstElementChild;
  var wasPlaying = !vid.paused;
  var time = vid.currentTime;
  // console.log(time);
  // vid.pause();
  // vid.src = vid.src.substr(0, vid.src.length - 7 ) + ".mp4#t=" + time;
  // vid.poster = "";
  // vid.load();
  // vid.addEventListener('loadedmetadata', jumpToTime, false);

  // function jumpToTime() {
  //   vid.removeEventListener('loadedmetadata', jumpToTime, false);
  //   // this.currentTime = time;
  //   if (wasPlaying) vid.play();
  // }
}



var videoBlocks = document.querySelectorAll('.video-block');
var videoModal = document.getElementById('js-video-modal');
var videoModalBG = document.getElementById('js-video-modal-bg');
var videoWrapper = videoModal.querySelector('.video-wrapper');
var videoClose = videoModal.querySelector('.close');

for (var i = 0; i < videoBlocks.length; i++) {
  videoBlocks[i].addEventListener('click', openModal, false);
}
videoModal.addEventListener('click', closeModal, false);
videoClose.addEventListener('click', closeModal, false);
videoWrapper.addEventListener('click', preventClickBubble, false);

function openModal(event) {
  document.body.classList.add("no-scroll");
  videoModal.style.transformOrigin = event.clientX + "px " + event.clientY + "px";

  if (this.querySelector('.ytvid')) {
    videoWrapper.classList.remove("square");
    var frame = document.createElement('iframe');
    frame.setAttribute("type", "text/html");
    frame.setAttribute("src", "https://www.youtube.com/embed/"+ this.querySelector('.ytvid').dataset.ytid +"?autoplay=1&modestbranding=1&playsinline=1&rel=0&showinfo=0&color=white");
    frame.setAttribute("frameborder", "0");
    frame.setAttribute("allowfullscreen", "");
    frame.setAttribute("allowfullscreen", "");
    videoWrapper.appendChild(frame);
  }else{
    videoWrapper.classList.add("square");
    var native = document.createElement('video');
    native.src = this.querySelector('video').src;
    native.setAttribute("type", "video/mp4");
    native.setAttribute("controls", "");
    native.setAttribute("playsinline", "");
    native.setAttribute("width", "600");
    videoWrapper.appendChild(native);
    videoModal.querySelector('video').load();
    videoModal.addEventListener("transitionend", beginVideo, false);  
  }
  videoModal.classList.remove("hidden");
  videoModalBG.classList.remove("hidden");
}

function preventClickBubble(event) {
  event.stopPropagation();
}

function beginVideo() {
  videoModal.removeEventListener("transitionend", beginVideo, false);
  videoModal.querySelector('video').play();
}

function closeModal() {
  
  setTimeout( function() {
    videoWrapper.removeChild(videoWrapper.lastElementChild);
  }, 500);

  document.body.classList.remove("no-scroll");
  videoModal.classList.add("hidden");
  videoModalBG.classList.add("hidden");
}

var drawers = document.querySelectorAll('.drawer');
for (var i = 0; i < drawers.length; i++) {
  drawers[i].addEventListener('click', toggleBox, false);
}

function toggleBox() {
  this.classList.toggle('open');
}