<?php
/**
 * Template Name: Resources
 *
 * @package Spike
 */

$fields = get_fields();

get_header();?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<section class="quote">
					<div class="contain">
						<img src="<?= get_stylesheet_directory_uri() . '/img/quotes/gorsuch.png';?>"/>
						<?php include( get_stylesheet_directory() . '/svg/quote-gorsuch.svg') ?>
					</div>
				</section>
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header><!-- .entry-header -->
			
				<div class="entry-content contain">
					<?php the_content(); ?>
				</div><!-- .entry-content -->
			</article><!-- #post-## -->
	
	<?php endwhile; // end of the loop. ?>
	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
