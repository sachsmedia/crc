<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Spike
 */

$fields = get_fields( get_option( 'page_for_posts' ) );

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			
		<section class="quote">
			<div class="contain">
				<img src="<?= get_stylesheet_directory_uri() . '/img/quotes/sotomayor.png';?>"/>
				<?php include( get_stylesheet_directory() . '/svg/quote-soto.svg') ?>
			</div>
		</section>

		<!-- <a class="news-updates" href="#sign-up"><button>Get updates</button></a> -->

		<div class="contain solo news">
			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<header class="entry-header">
							<a href="<?php the_permalink(); ?>"><h2 class="result-title"><?php the_title(); ?></h2></a>

							<div class="entry-meta">
								<?= get_the_date("F jS, Y");?>
							</div><!-- .entry-meta -->
						</header><!-- .entry-header -->

						<?php the_content(); ?>

					</article><!-- #post-## -->
				<?php endwhile; ?>

			<?php endif; ?>
		<?php spike_paging_nav(); ?>
		</div>
	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer("signup"); ?>
<?php get_footer(); ?>
