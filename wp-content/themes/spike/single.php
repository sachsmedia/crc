<?php
/**
 * The template for displaying all single posts.
 *
 * @package Spike
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main top-section" role="main">
			<?php if ( have_posts() ) : ?>
			
			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
			<header class="entry-header contain">
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header><!-- .entry-header -->
			<div class="contain solo">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-meta">
						<?= get_the_date("F jS, Y");?>
					</div><!-- .entry-meta -->
					
					<?php the_content(); ?>

				</article><!-- #post-## -->
			<?php endwhile; ?>
			<?php endif; ?>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>