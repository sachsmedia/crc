<?php
/**
 * Template Name: Home
 * @package Spike
 */

get_header();

$fields = get_fields();

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main home" role="main">

			<section class="hero top-section">
				<div class="contain">
					<div class="intro">
						<?php include( get_stylesheet_directory() . '/svg/logo.svg') ?>
						<?= $fields['lead'] ?>
						<?php if ( $fields['button_destination'] == "URL" ): ?>
							<a href="<?= $fields['button_url'] ?>" ><button><?= $fields['button_label'] ?></button></a>
						<?php else: ?>
							<a id="js-go-to-cta" href="#act" ><button><?= $fields['button_label'] ?></button></a>
						<?php endif; ?>
					</div>
					<div class="video-wrapper-contain">
						<div class="video-wrapper inline">
							<video playsinline controls controlsList="nodownload" 
										 poster="<?= get_stylesheet_directory_uri() . '/video/CRC-poster.jpg'?>"
										 src="<?= get_stylesheet_directory_uri() . '/video/CRC.mp4'?>" type="video/mp4">
								<track src="<?= get_stylesheet_directory_uri() . '/video/CRC.vtt'?>" kind="subtitles" srclang="en" label="English" />
								<!--<source src="<?= $fields['video'] ?>" type="video/mp4" />-->
								<!--<source src="" type="video/webm" />-->
							</video>
							<!--<span class="fullscreen">
								<svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M883 1056q0 13-10 23l-332 332 144 144q19 19 19 45t-19 45-45 19h-448q-26 0-45-19t-19-45v-448q0-26 19-45t45-19 45 19l144 144 332-332q10-10 23-10t23 10l114 114q10 10 10 23zm781-864v448q0 26-19 45t-45 19-45-19l-144-144-332 332q-10 10-23 10t-23-10l-114-114q-10-10-10-23t10-23l332-332-144-144q-19-19-19-45t19-45 45-19h448q26 0 45 19t19 45z" fill="#fff"/></svg>
							</span>-->
						</div>
					</div>
				</div>
			</section>

			<section class="quote">
				<div class="contain">
					<blockquote>
						<p><?= $fields['quote'] ?></p>
						<footer>– <cite><?= $fields['credit'] ?></cite></footer>
					</blockquote>
				</div>
			</section>
			
			<section class="content">
				<div class="contain">
					<div class="icon-split gavel">
						<?php include( get_stylesheet_directory() . '/svg/gavel.svg') ?>
						<div class="paragraph">
							<?= $fields['gavel_content'] ?>
						</div>
					</div>

					<div class="icon-split court-house">
						<div>
							<?= $fields['court_content'] ?>
						</div>
						<?php include( get_stylesheet_directory() . '/svg/court-house.svg') ?>
					</div>

					<hr/>

					<!--<div class="icon-split gavel">
						<?php include( get_stylesheet_directory() . '/svg/gavel.svg') ?>
						<div class="paragraph">
							<h2>If you had no idea...</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam corporis adipisci sequi, asperiores cum illum quo repellat deserunt ipsum ducimus officiis consequuntur nihil. Distinctio laboriosam, voluptate? Pariatur debitis consectetur, dolore. Voluptates commodi, maiores sed consequatur cumque, odit officiis illo! Asperiores doloribus culpa, totam labore quo dolore ut consequuntur, doloremque debitis, explicabo sapiente.</p>
						</div>
					</div>-->

				</div>
			</section>

			<section class="impact">
				<div class="contain">
					<?= $fields['blue_content'] ?>
				</div>
			</section>

			<?php if ($fields['show_videos'][0] == "show videos"): ?>
			<section class="video-gallary">
				<div class="contain">
					<?= do_shortcode('[videos amount="4"]'); ?>
				</div>
			</section>
			<?php endif;?>

			<section class="cta" id="act">
				<div class="contain">
					<?= $fields['cta_content'] ?>
				</div>
			</section>
			
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
