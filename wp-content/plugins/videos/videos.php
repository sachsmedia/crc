<?php
/*
Plugin Name: Videos Custom Post Type
Plugin URI: https://sachsmedia.com
Description: Creates a custom post type for the videos
Version: 1.0
Author: Mike Reinhard
Author URI: https://sachsmedia.com
License: GPLv2
*/

function custom_post_videos() {
  $labels = array(
    'name'               => _x( 'Videos', 'post type general name' ),
    'singular_name'      => _x( 'Video', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'Video' ),
    'add_new_item'       => __( 'Add New Video' ),
    'edit_item'          => __( 'Edit Video' ),
    'new_item'           => __( 'New Video' ),
    'all_items'          => __( 'Videos' ),
    'view_item'          => __( 'View Video' ),
    'search_items'       => __( 'Search Videos' ),
    'not_found'          => __( 'No Videos found' ),
    'not_found_in_trash' => __( 'No Videos found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Videos',
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'SMG Videos',
    'public'        => true,
    'menu_position' => 57,
    'menu_icon'			=> 'dashicons-format-video',
    'rewrite'				=> array('slug' => 'videos', 'with_front' => false),
    'supports'      => array( 'title' ),
    'has_archive'   => false
  );
  register_post_type( 'videos', $args ); 
}
add_action( 'init', 'custom_post_videos' );


// [bartag foo="foo-value"]
function videos_loop_func( $atts ) {
  $a = shortcode_atts( array(
      'amount' => 4
  ), $atts );

  $amount;
  if ( $a['amount'] == "all" ) $amount = -1;
  else $amount = (int) $a['amount'];

  $out = "";
	$loop = new WP_Query( array( 'post_type' => 'videos', 'posts_per_page' => $amount, 'order' => 'ASC' ));
  if ( $loop->have_posts() ) :
    $out .= '<div class="video-block-group">';
    while ( $loop->have_posts() ) : $loop->the_post();
      $src = get_field('url');
      $out .= '<div class="video-block" title="'. get_the_title() .'">';
      $out .=   '<div class="video-wrapper">';
      $out .=     build_video( $src );
      $out .=   '</div>';
      $out .= '</div>';
    endwhile;
    wp_reset_postdata();
    $out .= '</div>';
  endif;

  return $out;
}
add_shortcode( 'videos', 'videos_loop_func' );


function build_video( $src ) {
  $yt = parse_yturl($src);
  if ($yt) {
    return '<div class="ytvid" style="background-image:url(https://img.youtube.com/vi/'.$yt.'/hqdefault.jpg)" data-ytid="'.$yt.'"></div>';
  }else{
    return '<video src="'.$src.'" type="video/mp4"></video>';
  }
}

/**
 *  Check if input string is a valid YouTube URL
 *  and try to extract the YouTube Video ID from it.
 *  @author  Stephan Schmitz <eyecatchup@gmail.com>
 *  @param   $url   string   The string that shall be checked.
 *  @return  mixed           Returns YouTube Video ID, or (boolean) false.
 */        
function parse_yturl($url) {
  $pattern = '#^(?:https?://)?(?:www\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch\?v=|/watch\?.+&v=))([\w-]{11})(?:.+)?$#x';
  preg_match($pattern, $url, $matches);
  return (isset($matches[1])) ? $matches[1] : false;
}


?>